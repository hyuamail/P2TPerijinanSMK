package p2t.ijin.smk.p2tperijinansmk.model

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.io.Reader

@Entity
data class ChecklistDraft(
        @ColumnInfo @PrimaryKey var idChecklistDraft: Long = 0,
        @ColumnInfo var idDraftPengajuan: Long = 0,
        @ColumnInfo var idJenisPengajuan: Int = 0,
        @ColumnInfo var checklist: String = "",
        @ColumnInfo var fileName: String = "",
        @ColumnInfo var tglUpdate: String = ""
) {
    class Deserializer : ResponseDeserializable<ChecklistDraft> {
        override fun deserialize(reader: Reader) =
                Gson().fromJson(reader, ChecklistDraft::class.java)
    }

    class ListDeserializer : ResponseDeserializable<List<ChecklistDraft>> {
        override fun deserialize(reader: Reader): List<ChecklistDraft> {
            val type = object : TypeToken<List<ChecklistDraft>>() {}.type
            return Gson().fromJson(reader, type)
        }
    }
}