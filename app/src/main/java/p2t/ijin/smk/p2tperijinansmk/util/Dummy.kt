package p2t.ijin.smk.p2tperijinansmk.util

import p2t.ijin.smk.p2tperijinansmk.model.ChecklistDraft
import p2t.ijin.smk.p2tperijinansmk.model.ChecklistPengajuan
import p2t.ijin.smk.p2tperijinansmk.model.Pengajuan

object Dummy {

    val listPengajuan: List<Pengajuan>
        get() {
            var items: ArrayList<Pengajuan> = ArrayList()

            var item = Pengajuan(1, "420/536/116.6/2018",
                    "SMK YPI Darusalam 2 Cerme Kab. Gresik",
                    "Perpanjangan Ijin Operasional SMK",
                    "22 Januari 2018", "Pengajuan", "22 Januari 2018")
            items.add(item)

            item = Pengajuan(2, "420/536/116.6/2018",
                    "SMK YPI Darusalam 2 Cerme Kab. Gresik",
                    "Perubahan Program Keahlian SMK",
                    "22 Januari 2018", "Verval P2T", "22 Januari 2018")
            items.add(item)

            item = Pengajuan(3, "420/536/116.6/2018",
                    "Yayasan Pemuda Indonesia",
                    "Pendirian Sekolah Swasta",
                    "22 Januari 2018", "Penerbitan", "22 Januari 2018")
            items.add(item)

            item = Pengajuan(4, "420/536/116.6/2018",
                    "Yayasan Pemuda Indonesia",
                    "Pendirian Sekolah Swasta",
                    "22 Januari 2018", "Verval Disdik", "22 Januari 2018")
            items.add(item)

            item = Pengajuan(5, "420/536/116.6/2018",
                    "SMK YPI Darusalam 2 Cerme Kab. Gresik",
                    "Perubahan Program Keahlian SMK",
                    "22 Januari 2018", "Verval Cabdin", "22 Januari 2018")
            items.add(item)

            item = Pengajuan(6, "420/536/116.6/2018",
                    "SMK YPI Darusalam 2 Cerme Kab. Gresik",
                    "Perubahan Program Keahlian SMK",
                    "22 Januari 2018", "Draft", "22 Januari 2018")
            items.add(item)

            item = Pengajuan(7, "420/536/116.6/2018",
                    "Yayasan Pemuda Indonesia",
                    "Pendirian Sekolah Swasta",
                    "22 Januari 2018", "Ditolak", "22 Januari 2018")
            items.add(item)

            return items
        }

    val listChecklistPengajuan: List<ChecklistPengajuan>
        get() {
            var items: ArrayList<ChecklistPengajuan> = ArrayList()

            var item = ChecklistPengajuan(1, 1,
                    "Surat Permohonan izin kepada Gubernur / Administrator P2T",
                    "suratP2T.pdf",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(2, 1,
                    "Proposal pengajuan Perpanjangan Ijin Operasional",
                    "proposaloperasi.pdf",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(3, 1,
                    "Profil Sekolah",
                    "profil.pdf",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(4, 1,
                    "Data jumlah siswa 3 (tiga) tahun terakhir per kelas sesuai program / Kompetensi keahlian",
                    "jumlahsiswa.xlsx",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(5, 1,
                    "Jadwal pelajaran per tahun semester 1 (satu)",
                    "jumlahsiswa.xlsx",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(6, 1,
                    "Daftar nama Kepala Sekolah, Wakil Kepala Sekolah, guru dan karyawan yang dilampiri fotokopi ijazah",
                    "daftargurustaf.zip",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(7, 1,
                    "Fotocopi Nomor Pokok Sekolah Nasional (NPSN)",
                    "npsn.jpg",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(8, 1,
                    "Data Sarana Prasarana Sekolah",
                    "sarana.xlsx",
                    "")
            items.add(item)

            item = ChecklistPengajuan(9, 1,
                    "SK Pengangkatan Kepala Sekolah dari Yayasan",
                    "skkepala.jpg",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(10, 1,
                    "Fotocopi status tanah / akta tanah",
                    "tanah.jpg",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(11, 1,
                    "Fotokopi Akta Notaris Yayasan",
                    "akta.jpg",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(12, 1,
                    "Fotokopi pengesahan Yayasan dari Kemenkumham",
                    "sah.jpg",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(13, 1,
                    "Fotokopi SK Pendirian Sekolah",
                    "skberdiri.jpg",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(14, 1,
                    "Fotokopi Piagam Ijin Perpanjangan Sekolah swasta terakhir",
                    "ijinoperasi2016.jpg",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(15, 1,
                    "Fotokopi Sertifikasi Akreditasi Sekolah",
                    "akreditasi.jpg",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(16, 1,
                    "Fotokopi / printout rekening bank terbaru atas nama sekolah, 3 (tiga) bulan terakhir",
                    "rekening.pdf",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(17, 1,
                    "Surat pernyataan Kepala Sekolah tentang manajemen sekolah yang diketahui yayasan dan bermaterai Rp. 6000,-",
                    "pernyataan.jpg",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(18, 1,
                    "Fotokopi Ijin mendirikan Bangunan (IMB)",
                    "imb.jpg",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(19, 1,
                    "Instrumen supervisi dan Pengawas",
                    "intrumen.pdf",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(20, 2,
                    "Surat Permohonan izin kepada Gubernur / Administrator P2T",
                    "suratP2T.pdf",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(21, 2,
                    "Proposal pengajuan Perpanjangan Ijin Operasional",
                    "proposaloperasi.pdf",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(22, 2,
                    "Profil Sekolah",
                    "profil.pdf",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(23, 2,
                    "Data jumlah siswa 3 (tiga) tahun terakhir per kelas sesuai program / Kompetensi keahlian",
                    "jumlahsiswa.xlsx",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(24, 2,
                    "Jadwal pelajaran per tahun semester 1 (satu)",
                    "jumlahsiswa.xlsx",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(25, 2,
                    "Daftar nama Kepala Sekolah, Wakil Kepala Sekolah, guru dan karyawan yang dilampiri fotokopi ijazah",
                    "daftargurustaf.zip",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(26, 2,
                    "Fotocopi Nomor Pokok Sekolah Nasional (NPSN)",
                    "npsn.jpg",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(27, 2,
                    "Data Sarana Prasarana Sekolah",
                    "sarana.xlsx",
                    "")
            items.add(item)

            item = ChecklistPengajuan(28, 2,
                    "SK Pengangkatan Kepala Sekolah dari Yayasan",
                    "skkepala.jpg",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(29, 2,
                    "Fotocopi status tanah / akta tanah",
                    "tanah.jpg",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(30, 2,
                    "Fotokopi Akta Notaris Yayasan",
                    "akta.jpg",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(31, 3,
                    "Surat Permohonan izin kepada Gubernur / Administrator P2T",
                    "suratP2T.pdf",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(32, 3,
                    "Proposal pengajuan Perpanjangan Ijin Operasional",
                    "proposaloperasi.pdf",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(33, 3,
                    "Profil Sekolah",
                    "profil.pdf",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(34, 3,
                    "Data jumlah siswa 3 (tiga) tahun terakhir per kelas sesuai program / Kompetensi keahlian",
                    "jumlahsiswa.xlsx",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(35, 3,
                    "Jadwal pelajaran per tahun semester 1 (satu)",
                    "jumlahsiswa.xlsx",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(36, 3,
                    "Daftar nama Kepala Sekolah, Wakil Kepala Sekolah, guru dan karyawan yang dilampiri fotokopi ijazah",
                    "daftargurustaf.zip",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(37, 3,
                    "Fotocopi Nomor Pokok Sekolah Nasional (NPSN)",
                    "npsn.jpg",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(38, 3,
                    "Data Sarana Prasarana Sekolah",
                    "sarana.xlsx",
                    "")
            items.add(item)

            item = ChecklistPengajuan(39, 3,
                    "SK Pengangkatan Kepala Sekolah dari Yayasan",
                    "skkepala.jpg",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(40, 3,
                    "Fotocopi status tanah / akta tanah",
                    "tanah.jpg",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(41, 4,
                    "Surat Permohonan izin kepada Gubernur / Administrator P2T",
                    "suratP2T.pdf",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(42, 4,
                    "Proposal pengajuan Perpanjangan Ijin Operasional",
                    "proposaloperasi.pdf",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(43, 4,
                    "Profil Sekolah",
                    "profil.pdf",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(44, 4,
                    "Data jumlah siswa 3 (tiga) tahun terakhir per kelas sesuai program / Kompetensi keahlian",
                    "jumlahsiswa.xlsx",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(45, 4,
                    "Jadwal pelajaran per tahun semester 1 (satu)",
                    "jumlahsiswa.xlsx",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(46, 4,
                    "Daftar nama Kepala Sekolah, Wakil Kepala Sekolah, guru dan karyawan yang dilampiri fotokopi ijazah",
                    "daftargurustaf.zip",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(47, 4,
                    "Fotocopi Nomor Pokok Sekolah Nasional (NPSN)",
                    "npsn.jpg",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(48, 4,
                    "Data Sarana Prasarana Sekolah",
                    "sarana.xlsx",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(49, 4,
                    "SK Pengangkatan Kepala Sekolah dari Yayasan",
                    "skkepala.jpg",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(50, 4,
                    "Fotocopi status tanah / akta tanah",
                    "tanah.jpg",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(51, 5,
                    "Surat Permohonan izin kepada Gubernur / Administrator P2T",
                    "suratP2T.pdf",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(52, 5,
                    "Proposal pengajuan Perpanjangan Ijin Operasional",
                    "proposaloperasi.pdf",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(53, 5,
                    "Profil Sekolah",
                    "profil.pdf",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(54, 5,
                    "Data jumlah siswa 3 (tiga) tahun terakhir per kelas sesuai program / Kompetensi keahlian",
                    "jumlahsiswa.xlsx",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(55, 5,
                    "Jadwal pelajaran per tahun semester 1 (satu)",
                    "jumlahsiswa.xlsx",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(56, 5,
                    "Daftar nama Kepala Sekolah, Wakil Kepala Sekolah, guru dan karyawan yang dilampiri fotokopi ijazah",
                    "daftargurustaf.zip",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(57, 5,
                    "Fotocopi Nomor Pokok Sekolah Nasional (NPSN)",
                    "npsn.jpg",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(58, 5,
                    "Data Sarana Prasarana Sekolah",
                    "sarana.xlsx",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(59, 5,
                    "SK Pengangkatan Kepala Sekolah dari Yayasan",
                    "skkepala.jpg",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(60, 5,
                    "Fotocopi status tanah / akta tanah",
                    "tanah.jpg",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(61, 6,
                    "Surat Permohonan izin kepada Gubernur / Administrator P2T",
                    "suratP2T.pdf",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(62, 6,
                    "Proposal pengajuan Perpanjangan Ijin Operasional",
                    "",
                    "")
            items.add(item)

            item = ChecklistPengajuan(63, 6,
                    "Profil Sekolah",
                    "profil.pdf",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(64, 6,
                    "Data jumlah siswa 3 (tiga) tahun terakhir per kelas sesuai program / Kompetensi keahlian",
                    "ffg",
                    "")
            items.add(item)

            item = ChecklistPengajuan(65, 6,
                    "Jadwal pelajaran per tahun semester 1 (satu)",
                    "jumlahsiswa.xlsx",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(66, 6,
                    "Daftar nama Kepala Sekolah, Wakil Kepala Sekolah, guru dan karyawan yang dilampiri fotokopi ijazah",
                    "daftargurustaf.zip",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(67, 6,
                    "Fotocopi Nomor Pokok Sekolah Nasional (NPSN)",
                    "npsn.jpg",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(68, 6,
                    "Data Sarana Prasarana Sekolah",
                    "sarana.xlsx",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(69, 6,
                    "SK Pengangkatan Kepala Sekolah dari Yayasan",
                    "skkepala.jpg",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(70, 6,
                    "Fotocopi status tanah / akta tanah",
                    "tanah.jpg",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(71, 7,
                    "Surat Permohonan izin kepada Gubernur / Administrator P2T",
                    "suratP2T.pdf",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(72, 7,
                    "Proposal pengajuan Perpanjangan Ijin Operasional",
                    "ijin.pdf",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(73, 7,
                    "Profil Sekolah",
                    "profil.pdf",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(74, 7,
                    "Data jumlah siswa 3 (tiga) tahun terakhir per kelas sesuai program / Kompetensi keahlian",
                    "jumlahsiswa.xlsx",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(75, 7,
                    "Jadwal pelajaran per tahun semester 1 (satu)",
                    "jumlahsiswa.xlsx",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(76, 7,
                    "Daftar nama Kepala Sekolah, Wakil Kepala Sekolah, guru dan karyawan yang dilampiri fotokopi ijazah",
                    "daftargurustaf.zip",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(77, 7,
                    "Fotocopi Nomor Pokok Sekolah Nasional (NPSN)",
                    "npsn.jpg",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(78, 7,
                    "Data Sarana Prasarana Sekolah",
                    "sarana.xlsx",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(79, 7,
                    "SK Pengangkatan Kepala Sekolah dari Yayasan",
                    "skkepala.jpg",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistPengajuan(80, 7,
                    "Fotocopi status tanah / akta tanah",
                    "tanah.jpg",
                    "22 Januari 2018")
            items.add(item)

            return items
        }

    val listChecklistDraft: List<ChecklistDraft>
        get() {
            var items: ArrayList<ChecklistDraft> = ArrayList()

            var item = ChecklistDraft(1, 1, 1,
                    "Surat Permohonan izin kepada Gubernur / Administrator P2T",
                    "suratP2T.pdf",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistDraft(2, 1, 1,
                    "Proposal pengajuan Perpanjangan Ijin Operasional",
                    "",
                    "")
            items.add(item)

            item = ChecklistDraft(3, 1, 1,
                    "Profil Sekolah",
                    "",
                    "")
            items.add(item)

            item = ChecklistDraft(4, 1, 1,
                    "Data jumlah siswa 3 (tiga) tahun terakhir per kelas sesuai program / Kompetensi keahlian",
                    "",
                    "")
            items.add(item)

            item = ChecklistDraft(5, 1, 1,
                    "Jadwal pelajaran per tahun semester 1 (satu)",
                    "",
                    "")
            items.add(item)

            item = ChecklistDraft(6, 1, 1,
                    "Daftar nama Kepala Sekolah, Wakil Kepala Sekolah, guru dan karyawan yang dilampiri fotokopi ijazah",
                    "",
                    "")
            items.add(item)

            item = ChecklistDraft(7, 1, 1,
                    "Fotocopi Nomor Pokok Sekolah Nasional (NPSN)",
                    "",
                    "")
            items.add(item)

            item = ChecklistDraft(8, 1, 1,
                    "Data Sarana Prasarana Sekolah",
                    "",
                    "")
            items.add(item)

            item = ChecklistDraft(9, 1, 1,
                    "SK Pengangkatan Kepala Sekolah dari Yayasan",
                    "",
                    "")
            items.add(item)

            item = ChecklistDraft(10, 1, 1,
                    "Fotocopi status tanah / akta tanah",
                    "",
                    "")
            items.add(item)

            item = ChecklistDraft(11, 1, 1,
                    "Fotokopi Akta Notaris Yayasan",
                    "",
                    "")
            items.add(item)

            item = ChecklistDraft(12, 1, 1,
                    "Fotokopi pengesahan Yayasan dari Kemenkumham",
                    "",
                    "")
            items.add(item)

            item = ChecklistDraft(13, 1, 1,
                    "Fotokopi SK Pendirian Sekolah",
                    "",
                    "")
            items.add(item)

            item = ChecklistDraft(14, 1, 1,
                    "Fotokopi Piagam Ijin Perpanjangan Sekolah swasta terakhir",
                    "",
                    "")
            items.add(item)

            item = ChecklistDraft(15, 1, 1,
                    "Fotokopi Sertifikasi Akreditasi Sekolah",
                    "",
                    "")
            items.add(item)

            item = ChecklistDraft(16, 1, 1,
                    "Fotokopi / printout rekening bank terbaru atas nama sekolah, 3 (tiga) bulan terakhir",
                    "",
                    "")
            items.add(item)

            item = ChecklistDraft(17, 1, 1,
                    "Surat pernyataan Kepala Sekolah tentang manajemen sekolah yang diketahui yayasan dan bermaterai Rp. 6000,-",
                    "",
                    "")
            items.add(item)

            item = ChecklistDraft(18, 1, 1,
                    "Fotokopi Ijin mendirikan Bangunan (IMB)",
                    "",
                    "")
            items.add(item)

            item = ChecklistDraft(19, 1, 1,
                    "Instrumen supervisi dan Pengawas",
                    "",
                    "")
            items.add(item)

            item = ChecklistDraft(20, 1, 2,
                    "Surat Permohonan izin kepada Gubernur / Administrator P2T",
                    "suratP2T.pdf",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistDraft(21, 1, 2,
                    "Proposal pengajuan Perubahan Program Keahlian",
                    "",
                    "")
            items.add(item)

            item = ChecklistDraft(22, 1, 2,
                    "Profil Sekolah",
                    "",
                    "")
            items.add(item)

            item = ChecklistDraft(23, 1, 2,
                    "Profil Sekolah",
                    "",
                    "")
            items.add(item)

            item = ChecklistDraft(24, 1, 2,
                    "Data jumlah siswa 3 (tiga) tahun terakhir per kelas sesuai program / Kompetensi keahlian",
                    "",
                    "")
            items.add(item)

            item = ChecklistDraft(25, 1, 2,
                    "Jadwal pelajaran per tahun semester 1 (satu)",
                    "",
                    "")
            items.add(item)

            item = ChecklistDraft(26, 1, 2,
                    "Daftar nama Kepala Sekolah, Wakil Kepala Sekolah, guru dan karyawan yang dilampiri fotokopi ijazah",
                    "",
                    "")
            items.add(item)

            item = ChecklistDraft(27, 1, 2,
                    "Fotocopi Nomor Pokok Sekolah Nasional (NPSN)",
                    "",
                    "")
            items.add(item)

            item = ChecklistDraft(28, 1, 2,
                    "Data Sarana Prasarana Sekolah",
                    "",
                    "")
            items.add(item)

            item = ChecklistDraft(29, 1, 2,
                    "SK Pengangkatan Kepala Sekolah dari Yayasan",
                    "",
                    "")
            items.add(item)

            item = ChecklistDraft(30, 1, 2,
                    "Fotocopi status tanah / akta tanah",
                    "",
                    "")
            items.add(item)

            item = ChecklistDraft(31, 1, 3,
                    "Surat Permohonan izin kepada Gubernur / Administrator P2T",
                    "suratP2T.pdf",
                    "22 Januari 2018")
            items.add(item)

            item = ChecklistDraft(32, 1, 3,
                    "Proposal pengajuan Pendirian Sekolah Swasta",
                    "",
                    "")
            items.add(item)

            item = ChecklistDraft(33, 1, 3,
                    "Profil Sekolah",
                    "",
                    "")
            items.add(item)

            item = ChecklistDraft(34, 1, 3,
                    "Profil Sekolah",
                    "",
                    "")
            items.add(item)

            item = ChecklistDraft(35, 1, 3,
                    "Data jumlah siswa 3 (tiga) tahun terakhir per kelas sesuai program / Kompetensi keahlian",
                    "",
                    "")
            items.add(item)

            item = ChecklistDraft(36, 1, 3,
                    "Jadwal pelajaran per tahun semester 1 (satu)",
                    "",
                    "")
            items.add(item)

            item = ChecklistDraft(37, 1, 3,
                    "Daftar nama Kepala Sekolah, Wakil Kepala Sekolah, guru dan karyawan yang dilampiri fotokopi ijazah",
                    "",
                    "")
            items.add(item)

            item = ChecklistDraft(38, 1, 3,
                    "Fotocopi Nomor Pokok Sekolah Nasional (NPSN)",
                    "",
                    "")
            items.add(item)

            item = ChecklistDraft(39, 1, 3,
                    "Data Sarana Prasarana Sekolah",
                    "",
                    "")
            items.add(item)

            item = ChecklistDraft(40, 1, 3,
                    "SK Pengangkatan Kepala Sekolah dari Yayasan",
                    "",
                    "")
            items.add(item)

            item = ChecklistDraft(41, 1, 3,
                    "Fotocopi status tanah / akta tanah",
                    "",
                    "")
            items.add(item)

            return items
        }
}