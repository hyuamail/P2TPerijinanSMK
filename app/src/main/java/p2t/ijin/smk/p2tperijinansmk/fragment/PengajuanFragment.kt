package p2t.ijin.smk.p2tperijinansmk.fragment


import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_pengajuan.*
import kotlinx.android.synthetic.main.fragment_pengajuan.*
import p2t.ijin.smk.p2tperijinansmk.R
import p2t.ijin.smk.p2tperijinansmk.activity.FormAjuanActivity
import p2t.ijin.smk.p2tperijinansmk.activity.PengajuanProgressActivity
import p2t.ijin.smk.p2tperijinansmk.adapter.PengajuanAdapter
import p2t.ijin.smk.p2tperijinansmk.db.PengajuanViewModel
import p2t.ijin.smk.p2tperijinansmk.model.Pengajuan


class PengajuanFragment : Fragment() {

    private lateinit var mViewModel: PengajuanViewModel
    private lateinit var mAdapter: PengajuanAdapter


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_pengajuan, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        activity?.title = "Pengajuan Ijin"

        initFAB()

        initRecyclerView()

    }

    private fun initFAB() {
        fab.addOnMenuItemClickListener { _, _, itemId ->
            goToForm(itemId)
        }
    }

    private fun initRecyclerView() {
        recyclerViewPengajuan.layoutManager = LinearLayoutManager(context)

        mAdapter = PengajuanAdapter {
            goToForm(it.idPengajuan)
        }
        recyclerViewPengajuan.adapter = mAdapter

        recyclerViewPengajuan.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                //Log.d("FLOW","dy:${dy} - ${activity.navigation.isShown()}")
                activity?.navigation?.let {
                    if (dy > 0 && it.isShown) {
                        it.gone()
                        fab.gone()
                    } else if (dy < 0) {
                        it.visible()
                        fab.visible()
                    }
                }
            }
        })

        mViewModel = ViewModelProviders.of(this).get(PengajuanViewModel::class.java)
        mViewModel.items.observe(this, object : Observer<List<Pengajuan>> {

            override fun onChanged(list: List<Pengajuan>?) {
                list?.let { mAdapter.setList(it) }
                showEmptyMsg(list?.size ?: 0)
            }
        })
    }

    private fun showEmptyMsg(itemCount: Int) {
        if (itemCount == 0) {
            textViewEmpty.visible()
            recyclerViewPengajuan.gone()
        } else {
            textViewEmpty.gone()
            recyclerViewPengajuan.visible()
        }
    }

    private fun goToForm(idPengajuan: Long) {
        val intent = Intent(context, PengajuanProgressActivity::class.java)
        intent.putExtra(PengajuanProgressActivity.ID_IJIN, idPengajuan)
        startActivity(intent)
    }

    private fun goToForm(idJenis: Int) {
        val intent = Intent(activity, FormAjuanActivity::class.java)
        intent.putExtra(FormAjuanActivity.TYPE_IJIN, idJenis)
        startActivity(intent)
    }
}

fun View.visible() {
    visibility = View.VISIBLE
}

fun View.gone() {
    visibility = View.GONE
}
