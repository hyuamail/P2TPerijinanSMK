package p2t.ijin.smk.p2tperijinansmk.db

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import p2t.ijin.smk.p2tperijinansmk.model.ChecklistDraft


class ChecklistDraftViewModel(application: Application, val id: Int) :
        AndroidViewModel(application) {

    private val mRepository: PengajuanRepository

    var items: LiveData<List<ChecklistDraft>>

    init {
        mRepository = PengajuanRepository(application)
        items = mRepository.getChecklistDraft(id)
    }

    fun insert(checklist: ChecklistDraft) {
        mRepository.insertChecklistDraft(checklist)
    }
}