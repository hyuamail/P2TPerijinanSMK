package p2t.ijin.smk.p2tperijinansmk.db

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import p2t.ijin.smk.p2tperijinansmk.model.Pengajuan


class PengajuanViewModel(application: Application) : AndroidViewModel(application) {

    private val mRepository: PengajuanRepository

    var items: LiveData<List<Pengajuan>>

    init {
        mRepository = PengajuanRepository(application)
        items = mRepository.getOnProgress()
    }

    fun insert(pengajuan: Pengajuan) {
        mRepository.insert(pengajuan)
    }

    fun getPengajuan(id: Long): LiveData<Pengajuan> {
        return mRepository.getPengajuan(id)
    }
}