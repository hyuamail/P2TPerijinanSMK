package p2t.ijin.smk.p2tperijinansmk.db

import android.app.Application
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider

class ChecklistPengajuanViewModelFactory(val app: Application, val idPengajuan: Long) :
        ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ChecklistPengajuanViewModel(app, idPengajuan) as T
    }

}