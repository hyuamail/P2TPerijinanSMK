package p2t.ijin.smk.p2tperijinansmk.db

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import p2t.ijin.smk.p2tperijinansmk.model.ChecklistPengajuan


class ChecklistPengajuanViewModel(application: Application, val id: Long) :
        AndroidViewModel(application) {

    private val mRepository: PengajuanRepository

    var items: LiveData<List<ChecklistPengajuan>>

    init {
        mRepository = PengajuanRepository(application)
        items = mRepository.getChecklistPengajuan(id)
    }

    fun insert(checklistPengajuan: ChecklistPengajuan) {
        mRepository.insertChecklistPengajuan(checklistPengajuan)
    }
}