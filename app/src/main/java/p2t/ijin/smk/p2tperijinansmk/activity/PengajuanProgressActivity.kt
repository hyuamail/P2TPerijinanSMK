package p2t.ijin.smk.p2tperijinansmk.activity

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_pengajuanprogress.*
import kotlinx.android.synthetic.main.content_pengajuanprogress.*
import p2t.ijin.smk.p2tperijinansmk.R
import p2t.ijin.smk.p2tperijinansmk.adapter.ChecklistPengajuanAdapter
import p2t.ijin.smk.p2tperijinansmk.db.ChecklistPengajuanViewModel
import p2t.ijin.smk.p2tperijinansmk.db.ChecklistPengajuanViewModelFactory
import p2t.ijin.smk.p2tperijinansmk.db.PengajuanViewModel
import p2t.ijin.smk.p2tperijinansmk.fragment.gone
import p2t.ijin.smk.p2tperijinansmk.fragment.visible
import p2t.ijin.smk.p2tperijinansmk.model.ChecklistPengajuan
import p2t.ijin.smk.p2tperijinansmk.model.Pengajuan
import p2t.ijin.smk.p2tperijinansmk.util.IconUtil

class PengajuanProgressActivity : AppCompatActivity() {

    companion object {
        val ID_IJIN = "Id Ijin"
    }

    private lateinit var mViewModel: ChecklistPengajuanViewModel
    private lateinit var mAdapter: ChecklistPengajuanAdapter
    private lateinit var mItemViewModel: PengajuanViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pengajuanprogress)
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        initData()

        initRecyclerView()

    }

    private fun initData() {

        mItemViewModel = ViewModelProviders.of(this).get(PengajuanViewModel::class.java)
        mItemViewModel.getPengajuan(intent.getLongExtra(ID_IJIN, 0))
                .observe(this, object : Observer<Pengajuan> {

                    override fun onChanged(pengajuan: Pengajuan?) {
                        pengajuan?.let { bind(it) }
                    }
                })
    }

    fun bind(item: Pengajuan) {

        title = item.noPengajuan

        textViewStatus.text = "${item.status} - ${item.tglUpdate}"
        textViewPemohon.text = item.pemohon
        textViewJenisPengajuan.text = item.jenisPengajuan
        textViewNoPengajuan.text = item.noPengajuan
        textViewTglPengajuan.text = item.tglPengajuan
        textViewPemohon.text = item.pemohon

        stepViewStatus.go(when (item.status) {
            getString(R.string.statusDiajukan) -> 1
            getString(R.string.statusCabdin) -> 2
            getString(R.string.statusP2T) -> 3
            getString(R.string.statusDisdik) -> 4
            getString(R.string.statusSelesai) -> 4
            else -> 0
        }, false)

        stepViewStatus.done(item.status == getString(R.string.statusSelesai))

        changeIconJenis(item.jenisPengajuan)
    }

    private fun changeIconJenis(jenis: String) {
        imageViewJenis.setImageResource(IconUtil.getIcon(this, jenis))
    }

    private fun initRecyclerView() {
        recyclerViewFormAjuan.layoutManager = LinearLayoutManager(this)

        mAdapter = ChecklistPengajuanAdapter()
        recyclerViewFormAjuan.adapter = mAdapter

        mViewModel = ViewModelProviders.of(this, ChecklistPengajuanViewModelFactory(
                application, intent.getLongExtra(ID_IJIN, 0)))
                .get(ChecklistPengajuanViewModel::class.java)
        mViewModel.items.observe(this, object : Observer<List<ChecklistPengajuan>> {

            override fun onChanged(list: List<ChecklistPengajuan>?) {
                list?.let { mAdapter.setList(it) }
                showEmptyMsg(list?.size ?: 0)
            }
        })
    }

    private fun showEmptyMsg(itemCount: Int) {
        if (itemCount == 0) {
            textViewEmpty.visible()
            recyclerViewFormAjuan.gone()
        } else {
            textViewEmpty.gone()
            recyclerViewFormAjuan.visible()
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }

}
