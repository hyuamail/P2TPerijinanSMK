package p2t.ijin.smk.p2tperijinansmk.model

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson
import java.io.Reader

data class ResponsePengajuan(var record: List<Pengajuan>) {

    class Deserializer : ResponseDeserializable<ResponsePengajuan> {
        override fun deserialize(reader: Reader) =
                Gson().fromJson(reader, ResponsePengajuan::class.java)
    }
}