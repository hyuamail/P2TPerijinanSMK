package p2t.ijin.smk.p2tperijinansmk.util

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.drawable.ColorDrawable
import p2t.ijin.smk.p2tperijinansmk.R

/**
 * Created by hyuam on 18/09/2017.
 */
object ColorUtil {

    fun getColor(context: Context, status: String): ColorStateList {

        val color = when (status) {
            context.getString(R.string.statusDiajukan) -> ColorStateList.valueOf(
                    context.resources.getColor(R.color.colorDiajukan))
            context.getString(R.string.statusCabdin) -> ColorStateList.valueOf(
                    context.resources.getColor(R.color.colorCabdin))
            context.getString(R.string.statusP2T) -> ColorStateList.valueOf(
                    context.resources.getColor(R.color.colorP2T))
            context.getString(R.string.statusDisdik) -> ColorStateList.valueOf(
                    context.resources.getColor(R.color.colorDisdik))
            context.getString(R.string.statusSelesai) -> ColorStateList.valueOf(
                    context.resources.getColor(R.color.colorSelesai))
            context.getString(R.string.statusDraft) -> ColorStateList.valueOf(
                    context.resources.getColor(R.color.colorDraft))
            else -> ColorStateList.valueOf(context.resources.getColor(R.color.colorDitolak))
        }

        return color
    }

    fun getColorDrawable(context: Context, status: String): ColorDrawable {

        val color = when (status) {
            context.getString(R.string.statusDiajukan) -> ColorDrawable(
                    context.resources.getColor(R.color.colorDiajukan))
            context.getString(R.string.statusCabdin) -> ColorDrawable(
                    context.resources.getColor(R.color.colorCabdin))
            context.getString(R.string.statusP2T) -> ColorDrawable(
                    context.resources.getColor(R.color.colorP2T))
            context.getString(R.string.statusDisdik) -> ColorDrawable(
                    context.resources.getColor(R.color.colorDisdik))
            context.getString(R.string.statusSelesai) -> ColorDrawable(
                    context.resources.getColor(R.color.colorSelesai))
            context.getString(R.string.statusDraft) -> ColorDrawable(
                    context.resources.getColor(R.color.colorDraft))
            else -> ColorDrawable(context.resources.getColor(R.color.colorDitolak))
        }

        return color
    }
}