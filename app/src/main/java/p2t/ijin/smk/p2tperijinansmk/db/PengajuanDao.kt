package p2t.ijin.smk.p2tperijinansmk.db

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import p2t.ijin.smk.p2tperijinansmk.model.ChecklistDraft
import p2t.ijin.smk.p2tperijinansmk.model.ChecklistPengajuan
import p2t.ijin.smk.p2tperijinansmk.model.Pengajuan


@Dao
interface PengajuanDao {

    @Query("select * from Pengajuan")
    fun getAllPengajuan(): LiveData<List<Pengajuan>>

    @Query("select * from Pengajuan where status <> 'Penerbitan' AND status <> 'Ditolak'")
    fun getPengajuanOnProgress(): LiveData<List<Pengajuan>>

    @Query("select * from Pengajuan where status = 'Penerbitan' OR status = 'Ditolak'")
    fun getPengajuanFinish(): LiveData<List<Pengajuan>>

    @Query("select * from Pengajuan where idPengajuan = :id")
    fun getPengajuan(id: Long): LiveData<Pengajuan>

    @Insert(onConflict = REPLACE)
    fun addPengajuan(pengajuan: Pengajuan)

    @Update
    fun edit(pengajuan: Pengajuan)

    @Delete
    fun delete(pengajuan: Pengajuan)

    @Query("DELETE FROM Pengajuan")
    fun deleteAllPengajuan()

    @Query("select * from ChecklistPengajuan where idPengajuan = :id")
    fun getChecklistPengajuan(id: Long): LiveData<List<ChecklistPengajuan>>

    @Insert(onConflict = REPLACE)
    fun addChecklistPengajuan(checklistPengajuan: ChecklistPengajuan)

    @Query("DELETE FROM ChecklistPengajuan")
    fun deleteAllChecklistPengajuan()

    @Query("select * from ChecklistDraft where idJenisPengajuan = :id")
    fun getChecklistDraft(id: Int): LiveData<List<ChecklistDraft>>

    @Insert(onConflict = REPLACE)
    fun addChecklistDraft(checklistDraft: ChecklistDraft)

}