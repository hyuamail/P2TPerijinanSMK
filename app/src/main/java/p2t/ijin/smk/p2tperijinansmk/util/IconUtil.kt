package p2t.ijin.smk.p2tperijinansmk.util

import android.content.Context
import p2t.ijin.smk.p2tperijinansmk.R

/**
 * Created by hyuam on 18/09/2017.
 */
object IconUtil {

    fun getIcon(context: Context, jenis: String): Int {

        val icon = when (jenis) {
            context.getString(R.string.fabitem1) -> R.drawable.ic_autorenew_white_24dp
            context.getString(R.string.fabitem2) -> R.drawable.ic_iso_white_24dp
            context.getString(R.string.fabitem3) -> R.drawable.ic_school_white_24dp
            else -> R.drawable.ic_add_white_24dp
        }

        return icon
    }
}