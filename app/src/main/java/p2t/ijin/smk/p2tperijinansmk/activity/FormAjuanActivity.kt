package p2t.ijin.smk.p2tperijinansmk.activity

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import com.codekidlabs.storagechooser.StorageChooser
import kotlinx.android.synthetic.main.activity_form_ajuan.*
import kotlinx.android.synthetic.main.content_form_ajuan.*
import p2t.ijin.smk.p2tperijinansmk.R
import p2t.ijin.smk.p2tperijinansmk.adapter.ChecklistDraftAdapter
import p2t.ijin.smk.p2tperijinansmk.db.ChecklistDraftViewModel
import p2t.ijin.smk.p2tperijinansmk.db.ChecklistDraftViewModelFactory
import p2t.ijin.smk.p2tperijinansmk.fragment.gone
import p2t.ijin.smk.p2tperijinansmk.fragment.visible
import p2t.ijin.smk.p2tperijinansmk.model.ChecklistDraft

class FormAjuanActivity : AppCompatActivity() {

    companion object {
        val TYPE_IJIN = "Tipe Ijin"
    }

    private lateinit var mViewModel: ChecklistDraftViewModel
    private lateinit var mAdapter: ChecklistDraftAdapter

    private var mTypeIjin: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_form_ajuan)
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        title = when (intent.getIntExtra(TYPE_IJIN, 0)) {
            R.id.menufab1 -> {
                mTypeIjin = 1
                getString(R.string.fabitem1)
            }
            R.id.menufab2 -> {
                mTypeIjin = 2
                getString(R.string.fabitem2)
            }
            R.id.menufab3 -> {
                mTypeIjin = 3
                getString(R.string.fabitem3)
            }
            else -> getString(R.string.app_name)
        }

        initFAB()

        initRecyclerView()

    }

    private fun initFAB() {
        fabSend.setOnClickListener { view ->
            Snackbar.make(view, "Telah diajukan", Snackbar.LENGTH_LONG)
                    .setAction("Ajukan", null).show()
            //finish()
        }
    }

    private fun initRecyclerView() {
        recyclerViewFormAjuan.layoutManager = LinearLayoutManager(this)

        mAdapter = ChecklistDraftAdapter {
            goToBrowseFile(it)
        }
        recyclerViewFormAjuan.adapter = mAdapter

        recyclerViewFormAjuan.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                if (dy > 0 && fabSend.isShown) {
                    fabSend.gone()
                } else if (dy < 0) {
                    fabSend.visible()
                }
            }
        })

        mViewModel = ViewModelProviders.of(this, ChecklistDraftViewModelFactory(
                application, mTypeIjin)).get(ChecklistDraftViewModel::class.java)
        mViewModel.items.observe(this, object : Observer<List<ChecklistDraft>> {

            override fun onChanged(list: List<ChecklistDraft>?) {
                list?.let { mAdapter.setList(it) }
                showEmptyMsg(list?.size ?: 0)
            }
        })
    }

    private fun showEmptyMsg(itemCount: Int) {
        if (itemCount == 0) {
            textViewEmpty.visible()
            recyclerViewFormAjuan.gone()
        } else {
            textViewEmpty.gone()
            recyclerViewFormAjuan.visible()
        }
    }

    private fun goToBrowseFile(idJenis: ChecklistDraft) {
        // Initialize Builder
        val chooser = StorageChooser.Builder()
                .withActivity(this@FormAjuanActivity)
                .withFragmentManager(fragmentManager)
                .withMemoryBar(true)
                .allowCustomPath(true)
                .setType(StorageChooser.FILE_PICKER)
                .build()

        // get path that the user has chosen
        chooser.setOnSelectListener(object : StorageChooser.OnSelectListener {
            override fun onSelect(path: String) {
                Log.d("FLOWSELECTED_PATH", path)
            }
        })

        // Show dialog whenever you want by
        chooser.show()

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }

}
