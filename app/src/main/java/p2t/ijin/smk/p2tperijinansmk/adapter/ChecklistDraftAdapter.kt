package p2t.ijin.smk.p2tperijinansmk.adapter

import android.content.Context
import android.content.res.ColorStateList
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_checklist.view.*
import p2t.ijin.smk.p2tperijinansmk.R
import p2t.ijin.smk.p2tperijinansmk.model.ChecklistDraft


class ChecklistDraftAdapter(var listener: (ChecklistDraft) -> Unit) :
        RecyclerView.Adapter<ChecklistDraftAdapter.ViewHolder>() {

    var items: List<ChecklistDraft>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            ViewHolder(parent.inflate(R.layout.item_checklist))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
            holder.bind(items?.get(position) ?: ChecklistDraft(), listener)

    override fun getItemCount() = items?.size ?: 0

    fun setList(list: List<ChecklistDraft>) {
        items = list
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: ChecklistDraft, listener: (ChecklistDraft) -> Unit) = with(itemView) {
            textViewChecklist.text = item.checklist
            textViewFilename.text = item.fileName
            textViewTglUpdate.text = item.tglUpdate

            changeCardColor(item.fileName != "")

            setOnClickListener { listener(item) }
        }

        fun changeCardColor(exist: Boolean) {
            itemView.cardViewProgress.setCardBackgroundColor(
                    getColor(itemView.context, exist))
        }

        fun getColor(context: Context, exist: Boolean): ColorStateList {
            if (exist) return ColorStateList.valueOf(context.resources.getColor(R.color.colorP2T))
            else return ColorStateList.valueOf(context.resources.getColor(R.color.colorDitolak))
        }
    }
}

//fun ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false): View {
//    return LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)
//}