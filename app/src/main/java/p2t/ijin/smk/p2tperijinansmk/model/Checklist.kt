package p2t.ijin.smk.p2tperijinansmk.model

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.io.Reader

@Entity
data class Checklist(
        @ColumnInfo @PrimaryKey var idChecklist: Int = 0,
        @ColumnInfo var checklist: String = ""
) {
    class Deserializer : ResponseDeserializable<Checklist> {
        override fun deserialize(reader: Reader) = Gson().fromJson(reader, Checklist::class.java)
    }

    class ListDeserializer : ResponseDeserializable<List<Checklist>> {
        override fun deserialize(reader: Reader): List<Checklist> {
            val type = object : TypeToken<List<Checklist>>() {}.type
            return Gson().fromJson(reader, type)
        }
    }
}