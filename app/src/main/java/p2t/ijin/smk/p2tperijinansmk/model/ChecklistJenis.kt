package p2t.ijin.smk.p2tperijinansmk.model

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.io.Reader

@Entity
data class ChecklistJenis(
        @ColumnInfo @PrimaryKey var idChecklistJenis: Int = 0,
        @ColumnInfo var idJenis: Int = 0,
        @ColumnInfo var idChecklist: Int = 0,
        @ColumnInfo var tglCreate: String = ""
) {
    class Deserializer : ResponseDeserializable<ChecklistJenis> {
        override fun deserialize(reader: Reader) =
                Gson().fromJson(reader, ChecklistJenis::class.java)
    }

    class ListDeserializer : ResponseDeserializable<List<ChecklistJenis>> {
        override fun deserialize(reader: Reader): List<ChecklistJenis> {
            val type = object : TypeToken<List<ChecklistJenis>>() {}.type
            return Gson().fromJson(reader, type)
        }
    }
}