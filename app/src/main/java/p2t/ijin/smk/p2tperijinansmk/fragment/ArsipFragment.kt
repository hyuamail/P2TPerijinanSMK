package p2t.ijin.smk.p2tperijinansmk.fragment


import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_arsip.*
import p2t.ijin.smk.p2tperijinansmk.R
import p2t.ijin.smk.p2tperijinansmk.activity.PengajuanProgressActivity
import p2t.ijin.smk.p2tperijinansmk.adapter.PengajuanAdapter
import p2t.ijin.smk.p2tperijinansmk.db.ArsipViewModel
import p2t.ijin.smk.p2tperijinansmk.model.Pengajuan


class ArsipFragment : Fragment() {

    private lateinit var mViewModel: ArsipViewModel
    private lateinit var mAdapter: PengajuanAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_arsip, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        activity?.title = "Arsip Pengajuan"

        initRecyclerView()
    }

    private fun initRecyclerView() {
        recyclerViewArsip.layoutManager = LinearLayoutManager(context)

        mAdapter = PengajuanAdapter {
            goToForm(it.idPengajuan)
        }
        recyclerViewArsip.adapter = mAdapter

        recyclerViewArsip.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                //Log.d("FLOW","dy:${dy} - ${activity.navigation.isShown()}")
                activity?.navigation?.let {
                    if (dy > 0 && it.isShown) {
                        it.gone()
                    } else if (dy < 0) {
                        it.visible()
                    }
                }
            }
        })

        mViewModel = ViewModelProviders.of(this).get(ArsipViewModel::class.java)
        mViewModel.items.observe(this, object : Observer<List<Pengajuan>> {

            override fun onChanged(list: List<Pengajuan>?) {
                list?.let { mAdapter.setList(it) }
                showEmptyMsg(list?.size ?: 0)
            }
        })
    }

    private fun showEmptyMsg(itemCount: Int) {
        if (itemCount == 0) {
            textViewEmpty.visible()
            recyclerViewArsip.gone()
        } else {
            textViewEmpty.gone()
            recyclerViewArsip.visible()
        }
    }

    private fun goToForm(idPengajuan: Long) {
        val intent = Intent(activity, PengajuanProgressActivity::class.java)
        intent.putExtra(PengajuanProgressActivity.ID_IJIN, idPengajuan)
        startActivity(intent)
    }
}
