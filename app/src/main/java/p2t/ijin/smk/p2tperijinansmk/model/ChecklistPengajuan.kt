package p2t.ijin.smk.p2tperijinansmk.model

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.io.Reader

@Entity
data class ChecklistPengajuan(
        @ColumnInfo @PrimaryKey var idChecklistPengajuan: Long = 0,
        @ColumnInfo var idPengajuan: Long = 0,
        @ColumnInfo var checklist: String = "",
        @ColumnInfo var fileName: String = "",
        @ColumnInfo var tglUpdate: String = ""
) {
    class Deserializer : ResponseDeserializable<ChecklistPengajuan> {
        override fun deserialize(reader: Reader) =
                Gson().fromJson(reader, ChecklistPengajuan::class.java)
    }

    class ListDeserializer : ResponseDeserializable<List<ChecklistPengajuan>> {
        override fun deserialize(reader: Reader): List<ChecklistPengajuan> {
            val type = object : TypeToken<List<ChecklistPengajuan>>() {}.type
            return Gson().fromJson(reader, type)
        }
    }
}