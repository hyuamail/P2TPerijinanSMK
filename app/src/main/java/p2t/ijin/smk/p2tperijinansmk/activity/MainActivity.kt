package p2t.ijin.smk.p2tperijinansmk.activity

import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import p2t.ijin.smk.p2tperijinansmk.R
import p2t.ijin.smk.p2tperijinansmk.fragment.ArsipFragment
import p2t.ijin.smk.p2tperijinansmk.fragment.PengajuanFragment
import p2t.ijin.smk.p2tperijinansmk.fragment.ProfilFragment

class MainActivity : AppCompatActivity() {

    private val mOnNavigationItemSelectedListener =
            BottomNavigationView.OnNavigationItemSelectedListener { item ->
                container.scrollTo(0, 0)
                when (item.itemId) {
                    R.id.navigation_pengajuan -> {
                        changeToPengajuan()
                        return@OnNavigationItemSelectedListener true
                    }
                    R.id.navigation_arsip -> {
                        changeToArsip()
                        return@OnNavigationItemSelectedListener true
                    }
                    R.id.navigation_profil -> {
                        changeToProfil()
                        return@OnNavigationItemSelectedListener true
                    }
                }
                false
            }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

        navigation.setOnNavigationItemReselectedListener { container.scrollTo(0, 0) }

        changeToPengajuan()
    }

    private fun changeToPengajuan() {
        changePage(PengajuanFragment())
    }

    private fun changeToArsip() {
        changePage(ArsipFragment())
    }

    private fun changeToProfil() {
        changePage(ProfilFragment())
    }

    private fun changePage(frag: Fragment) {
        supportFragmentManager.beginTransaction()
                .replace(R.id.container, frag).commitNow()
    }
}
