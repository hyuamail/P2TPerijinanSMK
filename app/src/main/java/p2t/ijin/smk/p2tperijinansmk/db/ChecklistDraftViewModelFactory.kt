package p2t.ijin.smk.p2tperijinansmk.db

import android.app.Application
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider

class ChecklistDraftViewModelFactory(val app: Application, val idJenisPengajuan: Int) :
        ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ChecklistDraftViewModel(app, idJenisPengajuan) as T
    }

}