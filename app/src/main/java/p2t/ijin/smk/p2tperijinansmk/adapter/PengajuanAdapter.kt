package p2t.ijin.smk.p2tperijinansmk.adapter

import android.support.annotation.LayoutRes
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_pengajuan.view.*
import p2t.ijin.smk.p2tperijinansmk.R
import p2t.ijin.smk.p2tperijinansmk.model.Pengajuan
import p2t.ijin.smk.p2tperijinansmk.util.ColorUtil
import p2t.ijin.smk.p2tperijinansmk.util.IconUtil


class PengajuanAdapter(var listener: (Pengajuan) -> Unit) :
        RecyclerView.Adapter<PengajuanAdapter.ViewHolder>() {

    var items: List<Pengajuan>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            ViewHolder(parent.inflate(R.layout.item_pengajuan))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
            holder.bind(items?.get(position) ?: Pengajuan(), listener)

    override fun getItemCount() = items?.size ?: 0

    fun setList(list: List<Pengajuan>) {
        items = list
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: Pengajuan, listener: (Pengajuan) -> Unit) = with(itemView) {
            textViewStatus.text = "${item.status} - ${item.tglUpdate}"
            textViewPemohon.text = item.pemohon
            textViewJenisPengajuan.text = item.jenisPengajuan
            textViewNoPengajuan.text = item.noPengajuan
            textViewTglPengajuan.text = item.tglPengajuan
            textViewPemohon.text = item.pemohon

            stepViewStatus.go(when (item.status) {
                itemView.context.getString(R.string.statusDiajukan) -> 1
                itemView.context.getString(R.string.statusCabdin) -> 2
                itemView.context.getString(R.string.statusP2T) -> 3
                itemView.context.getString(R.string.statusDisdik) -> 4
                itemView.context.getString(R.string.statusSelesai) -> 4
                else -> 0
            }, false)

            stepViewStatus.done(
                    item.status == itemView.context.getString(R.string.statusSelesai))

            changeCardColor(item.status)
            changeIconJenis(item.jenisPengajuan)

            setOnClickListener { listener(item) }
        }

        private fun changeIconJenis(jenis: String) {
            itemView.imageViewJenis.setImageResource(IconUtil.getIcon(itemView.context, jenis))
        }

        fun changeCardColor(status: String) {
            itemView.cardViewProgress.setCardBackgroundColor(
                    ColorUtil.getColor(itemView.context, status))
        }
    }
}

fun ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false): View {
    return LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)
}