package p2t.ijin.smk.p2tperijinansmk.db

import android.app.Application
import android.arch.lifecycle.LiveData
import android.os.AsyncTask
import p2t.ijin.smk.p2tperijinansmk.model.ChecklistDraft
import p2t.ijin.smk.p2tperijinansmk.model.ChecklistPengajuan
import p2t.ijin.smk.p2tperijinansmk.model.Pengajuan


class PengajuanRepository(app: Application) {

    private lateinit var mDao: PengajuanDao
    //private lateinit var mItems: LiveData<List<Pengajuan>>
    //private lateinit var mItemsOnProgress: LiveData<List<Pengajuan>>
    //private lateinit var mItemsFinish: LiveData<List<Pengajuan>>

    init {
        val db = PengajuanDB.getDB(app)
        db?.let {
            mDao = it.pengajuanDao()
            //mItems = mDao.getAllPengajuan()
            //mItemsOnProgress = mDao.getPengajuanOnProgress()
            //mItemsFinish = mDao.getPengajuanFinish()
        }
    }

//    fun getAll(): LiveData<List<Pengajuan>> {
//        return mItems
//    }

    fun getOnProgress(): LiveData<List<Pengajuan>> {
        return mDao.getPengajuanOnProgress()
    }

    fun getFinish(): LiveData<List<Pengajuan>> {
        return mDao.getPengajuanFinish()
    }

    fun insert(item: Pengajuan) {
        insertAsyncTask(mDao).execute(item)
    }

    private class insertAsyncTask internal constructor(private val mTaskDao: PengajuanDao) :
            AsyncTask<Pengajuan, Void, Void>() {

        override fun doInBackground(vararg params: Pengajuan): Void? {
            mTaskDao.addPengajuan(params[0])
            return null
        }
    }

    fun getChecklistPengajuan(id: Long): LiveData<List<ChecklistPengajuan>> {
        return mDao.getChecklistPengajuan(id)
    }

    fun insertChecklistPengajuan(item: ChecklistPengajuan) {
        insertChecklistPengajuanAsyncTask(mDao).execute(item)
    }

    private class insertChecklistPengajuanAsyncTask internal constructor(
            private val mTaskDao: PengajuanDao) :
            AsyncTask<ChecklistPengajuan, Void, Void>() {

        override fun doInBackground(vararg params: ChecklistPengajuan): Void? {
            mTaskDao.addChecklistPengajuan(params[0])
            return null
        }
    }

    fun getChecklistDraft(id: Int): LiveData<List<ChecklistDraft>> {
        return mDao.getChecklistDraft(id)
    }

    fun insertChecklistDraft(item: ChecklistDraft) {
        insertChecklistDraftAsyncTask(mDao).execute(item)
    }

    private class insertChecklistDraftAsyncTask internal constructor(
            private val mTaskDao: PengajuanDao) :
            AsyncTask<ChecklistDraft, Void, Void>() {

        override fun doInBackground(vararg params: ChecklistDraft): Void? {
            mTaskDao.addChecklistDraft(params[0])
            return null
        }
    }

    fun getPengajuan(id: Long): LiveData<Pengajuan> {
        return mDao.getPengajuan(id)
    }

}