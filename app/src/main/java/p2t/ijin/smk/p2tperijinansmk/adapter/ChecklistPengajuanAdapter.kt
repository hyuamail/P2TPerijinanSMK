package p2t.ijin.smk.p2tperijinansmk.adapter

import android.content.Context
import android.content.res.ColorStateList
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_checklist.view.*
import p2t.ijin.smk.p2tperijinansmk.R
import p2t.ijin.smk.p2tperijinansmk.model.ChecklistPengajuan


class ChecklistPengajuanAdapter :
        RecyclerView.Adapter<ChecklistPengajuanAdapter.ViewHolder>() {

    var items: List<ChecklistPengajuan>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            ViewHolder(parent.inflate(R.layout.item_checklistpengajuan))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
            holder.bind(items?.get(position) ?: ChecklistPengajuan())

    override fun getItemCount() = items?.size ?: 0

    fun setList(list: List<ChecklistPengajuan>) {
        items = list
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: ChecklistPengajuan) = with(itemView) {
            textViewChecklist.text = item.checklist
            textViewFilename.text = item.fileName
            textViewTglUpdate.text = item.tglUpdate

            changeCardColor(item.fileName != "")
        }

        fun changeCardColor(exist: Boolean) {
            itemView.cardViewProgress.setCardBackgroundColor(
                    getColor(itemView.context, exist))
        }

        fun getColor(context: Context, exist: Boolean): ColorStateList {
            if (exist) return ColorStateList.valueOf(context.resources.getColor(R.color.colorP2T))
            else return ColorStateList.valueOf(context.resources.getColor(R.color.colorDitolak))
        }
    }
}

//fun ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false): View {
//    return LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)
//}