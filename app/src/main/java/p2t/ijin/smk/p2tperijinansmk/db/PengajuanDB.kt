package p2t.ijin.smk.p2tperijinansmk.db

import android.arch.persistence.db.SupportSQLiteDatabase
import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import android.os.AsyncTask
import p2t.ijin.smk.p2tperijinansmk.model.ChecklistDraft
import p2t.ijin.smk.p2tperijinansmk.model.ChecklistPengajuan
import p2t.ijin.smk.p2tperijinansmk.model.Pengajuan
import p2t.ijin.smk.p2tperijinansmk.util.Dummy


@Database(entities = arrayOf(
        Pengajuan::class, ChecklistPengajuan::class, ChecklistDraft::class),
        version = 1, exportSchema = false)
abstract class PengajuanDB : RoomDatabase() {

    abstract fun pengajuanDao(): PengajuanDao

    companion object {
        private var INSTANCE: PengajuanDB? = null

        fun getDB(context: Context): PengajuanDB? {
            if (INSTANCE == null) {
                synchronized(PengajuanDB::class) {
                    INSTANCE = Room.databaseBuilder(context.applicationContext,
                            PengajuanDB::class.java, "pengajuan.db")
                            .addCallback(sPengajuanDBCallback)
                            .build()
                }
            }
            return INSTANCE
        }

        fun destroyInstance() {
            INSTANCE = null
        }

        private val sPengajuanDBCallback = object : RoomDatabase.Callback() {

            override fun onOpen(db: SupportSQLiteDatabase) {
                super.onOpen(db)
                INSTANCE?.let { PopulateDbAsync(it).execute() }
            }
        }
    }

    private class PopulateDbAsync internal constructor(db: PengajuanDB) :
            AsyncTask<Void, Void, Void>() {

        private val mDao: PengajuanDao

        init {
            mDao = db.pengajuanDao()
        }

        override fun doInBackground(vararg params: Void): Void? {
            mDao.deleteAllPengajuan()
            var items = Dummy.listPengajuan
            for (item in items)
                mDao.addPengajuan(item)
            mDao.deleteAllChecklistPengajuan()
            var items2 = Dummy.listChecklistPengajuan
            for (item in items2)
                mDao.addChecklistPengajuan(item)
            var items3 = Dummy.listChecklistDraft
            for (item in items3)
                mDao.addChecklistDraft(item)
            return null
        }
    }
}