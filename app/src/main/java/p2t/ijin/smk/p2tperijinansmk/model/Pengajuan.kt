package p2t.ijin.smk.p2tperijinansmk.model

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.io.Reader

@Entity
data class Pengajuan(
        @ColumnInfo @PrimaryKey var idPengajuan: Long = 0,
        @ColumnInfo var noPengajuan: String = "",
        @ColumnInfo var pemohon: String = "",
        @ColumnInfo var jenisPengajuan: String = "",
        @ColumnInfo var tglPengajuan: String = "",
        @ColumnInfo var status: String = "",
        @ColumnInfo var tglUpdate: String = ""
) {
    class Deserializer : ResponseDeserializable<Pengajuan> {
        override fun deserialize(reader: Reader) = Gson().fromJson(reader, Pengajuan::class.java)
    }

    class ListDeserializer : ResponseDeserializable<List<Pengajuan>> {
        override fun deserialize(reader: Reader): List<Pengajuan> {
            val type = object : TypeToken<List<Pengajuan>>() {}.type
            return Gson().fromJson(reader, type)
        }
    }
}